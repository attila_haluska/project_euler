//
// Created by attilahaluska on 5/31/18.
//

#ifndef PROJECT_EULER_PROBLEM_0004_H
#define PROJECT_EULER_PROBLEM_0004_H

#include <cstdint>
#include <cstdbool>

constexpr bool is_palindrome(uint64_t number) {
    uint64_t reversed = 0;
    uint64_t n = number;

    while (n > 0) {
        uint64_t digit = n % 10;
        reversed = reversed * 10 + digit;
        n /= 10;
    }

    return number == reversed;
}

constexpr auto solve_problem_0004() -> uint64_t {
    uint64_t result = 0;

    for (int i = 999; i > 99; i--) {
        for (int j = 999; j >= i; j--) {
            uint64_t number = i * j;

            if (is_palindrome(number) && number > result) {
                result = number;
            }
        }
    }

    return result;
}

#endif //PROJECT_EULER_PROBLEM_0004_H
