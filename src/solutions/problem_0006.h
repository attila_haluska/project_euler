
//
// Created by attilahaluska on 6/1/18.
//

#ifndef PROJECT_EULER_PROBLEM_0006_H
#define PROJECT_EULER_PROBLEM_0006_H

#include <cstdint>
#include <cmath>

constexpr auto square_of_sums(uint64_t number) -> uint64_t {
	uint64_t sum = number * (number + 1) / 2;
	return std::pow(sum, 2);
}

constexpr auto sum_of_squares(uint64_t number) -> uint64_t {
	return (number * (number + 1) * ((2 * number) + 1)) / 6;
}

constexpr auto solve_problem_0006() -> uint64_t {
    return square_of_sums(100) - sum_of_squares(100);
}

#endif //PROJECT_EULER_PROBLEM_0006_H
