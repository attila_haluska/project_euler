//
// Created by attilahaluska on 5/29/18.
//

#ifndef PROJECT_EULER_PROBLEM_0002_H
#define PROJECT_EULER_PROBLEM_0002_H

#include <cstdint>

constexpr static auto solve_problem_0002() -> uint64_t {
    const uint64_t limit = 4000000;
    uint64_t a = 1;
    uint64_t b = 1;
    uint64_t c = a + b;
    uint64_t sum = 0;

    while (c < limit) {
        sum += c;
        a = b + c;
        b = a + c;
        c = a + b;
    }

    return sum;
}

#endif //PROJECT_EULER_PROBLEM_0002_H
