
//
// Created by attilahaluska on 5/31/18.
//

#ifndef PROJECT_EULER_PROBLEM_0005_H
#define PROJECT_EULER_PROBLEM_0005_H

#include <cstdint>

constexpr bool is_divisible(uint64_t number) {
	return number % 2 == 0 &&
		   number % 3 == 0 &&
		   number % 4 == 0 &&
		   number % 5 == 0 &&
		   number % 6 == 0 &&
		   number % 7 == 0 &&
		   number % 8 == 0 &&
		   number % 9 == 0 &&
		   number % 10 == 0 &&
		   number % 11 == 0 &&
		   number % 12 == 0 &&
		   number % 13 == 0 &&
		   number % 14 == 0 &&
		   number % 15 == 0 &&
		   number % 16 == 0 &&
		   number % 17 == 0 &&
		   number % 18 == 0 &&
		   number % 19 == 0 &&
		   number % 20 == 0;
}

constexpr auto solve_problem_0005() -> uint64_t {
    uint64_t result = 20;

    while (1) {
    	if (is_divisible(result)) {
    		return result;
    	}

    	result++;
    }

    return 0;
}

#endif //PROJECT_EULER_PROBLEM_0005_H
