//
// Created by attilahaluska on 5/31/18.
//

#ifndef PROJECT_EULER_PROBLEM_0003_H
#define PROJECT_EULER_PROBLEM_0003_H

#include <cstdint>

constexpr auto solve_problem_0003() -> uint64_t {
    uint64_t number = 600851475143ul;

    while (number % 2 == 0) {
        number /= 2;
    }

    uint64_t result = 2;
    uint64_t factor = 3;

    while (factor * factor <= number) {
        if (number % factor == 0) {
            if (factor > result) {
                result = factor;
            }

            number /= factor;
        } else {
            factor += 2;
        }
    }

    if (number != 1) {
        return number;
    }

    return result;
}

#endif //PROJECT_EULER_PROBLEM_0003_H
