//
// Created by attilahaluska on 5/29/18.
//

#ifndef PROJECT_EULER_PROBLEM_0001_H
#define PROJECT_EULER_PROBLEM_0001_H

constexpr static auto sum_divisable_by(const uint64_t limit, const uint64_t n) -> uint64_t {
    auto p = limit / n;
    return n * (p * (p + 1)) / 2;
}

constexpr static auto solve_problem_0001() -> uint64_t {
    const uint64_t limit = 999;
    return sum_divisable_by(limit, 3) + sum_divisable_by(limit, 5) - sum_divisable_by(limit, 15);
}

#endif //PROJECT_EULER_PROBLEM_0001_H
