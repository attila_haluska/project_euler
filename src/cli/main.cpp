//
// Created by attilahaluska on 5/19/18.
//

#include <iostream>

#include "solutions/problem_0001.h"
#include "solutions/problem_0002.h"
#include "solutions/problem_0003.h"
#include "solutions/problem_0004.h"
#include "solutions/problem_0005.h"
#include "solutions/problem_0006.h"

auto main(int argc, char** argv) -> int {
    using namespace std;

    cout << "Project Euler problem solutions" << endl << endl;
    
    cout << "Solution of problem 0001: " << solve_problem_0001() << endl;
    cout << "Solution of problem 0002: " << solve_problem_0002() << endl;
    cout << "Solution of problem 0003: " << solve_problem_0003() << endl;
    cout << "Solution of problem 0004: " << solve_problem_0004() << endl;
    cout << "Solution of problem 0005: " << solve_problem_0005() << endl;
    cout << "Solution of problem 0006: " << solve_problem_0006() << endl;

    return 0;
}
